Поиск номеров телефонов из питоновского списка и postgreSQL базы данных.
 
 Для развёртки проекта:
 
 - Создать среду ```$ virtualenv env``` 
 
 - Активировать среду ```$ source env/bin/activate```
  
 - Установить зависимости ```$ pip install -r requirements.txt```
 
 - Запустить скрипт ```$ python PhoneNumberTask.py```
