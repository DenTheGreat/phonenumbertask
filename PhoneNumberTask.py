import re
import unittest
import psycopg2

numbers = [
    '380675674432',
    '380672832500',
    '380983567721',
    '480380679812',
           ]

def find(item,list):
    return [i for i in list if re.search('^'+item, i)]

def find_db(item):
    cur.execute(f"SELECT phone FROM phonenumbers WHERE phone LIKE '{item}%' LIMIT 10;")
    return cur.fetchall()

class Test(unittest.TestCase): 
    def test_re_1(self):
        self.assertEqual(find('380',numbers), ['380675674432','380672832500','380983567721'])
        
    def test_re_2(self):
        self.assertEqual(find('38067',numbers), ['380675674432','380672832500'])
        
    def test_re_3(self):
        self.assertEqual(find('38098',numbers), ['380983567721',])       
        
    def test_re_4(self):
        self.assertEqual(find('490',numbers), [])
        
    def test_db_1(self):
        self.assertEqual(find_db('380'), [('380675674432',), ('380672832500',), ('380983567721',), ('380633567721',), ('380633567722',), ('380633567723',), ('380633567745',), ('380633576745',), ('380633578845',), ('380633578849',)])
        
    def test_db_2(self):
        self.assertEqual(find_db('38067'), [('380675674432',),('380672832500',)])
        
    def test_db_3(self):
        self.assertEqual(find_db('38098'), [('380983567721',)])       
        
    def test_db_4(self):
        self.assertEqual(find_db('490'), [])
        
if __name__ == '__main__':
    conn = psycopg2.connect(dbname="xdxhkzks", user="xdxhkzks", password="QauXt6k8m5FgSlRYPfPTSzOGVT3BX6Xi",host="dumbo.db.elephantsql.com")
    cur = conn.cursor()
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    unittest.TextTestRunner(verbosity=2).run(suite)
    cur.close()
    conn.close()